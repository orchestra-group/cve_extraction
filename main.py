import pandas as pd
import os
import json
from preprocessing import preprocessing
import pickle
import argparse




# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--text_file', default="None", help='inset the input text file')
    args = parser.parse_args()
    if not os.path.exists(args.text_file):
        print("dont find file , please check again")
        exit(1)
    file = open(args.text_file, 'r')
    data_file = json.load(file)
    file.close()
    print("start with preprocessing")
    dataframe, original_dataframe = preprocessing(data_file)
    print(original_dataframe)
    print("finish with preprocessing")
    dataframe = dataframe.drop('Code_Execution', axis=1)
    file = open('model_cve.pkl', 'rb')
    model = pickle.load(file)
    file.close()
    y = model.predict(dataframe)
    file = open('dict_cve_mapping.json', 'r')
    data_file = json.load(file)
    data_file = {int(k): v for k,v in data_file.items()}
    file.close()
    dataframe['Code_Execution'] = list(map(data_file.get, y))
    dataframe.filter(list(original_dataframe)).to_csv('result.csv', index=False)
