import json
from nltk.corpus import stopwords
from rake_nltk import Rake
import string

MAX_TEXTUAL_FEATURE = 17

def histogram(bags):
    hist = {}
    for i in bags:
        if i in hist:
            hist[i] += 1
        else:
            hist[i] = 1
    return hist


def normalize_text(keywords):
    tmp = keywords.copy()
    for i in range(len(tmp)):
        tmp[i] = tmp[i].translate(str.maketrans('', '', string.punctuation))
        check_first_digit = tmp[i].split()
        if not check_first_digit:
            continue
        if check_first_digit[0].isdigit() or check_first_digit[0].isspace():
            tmp[i] = " ".join(check_first_digit[1:])
    for key in tmp:
        if not key:
            tmp.remove(key)
    return tmp


def create_bags_of_words(dataframe):
    extractor = Rake()
    bags_words = []
    stop_words = set(stopwords.words('english'))
    for description in dataframe['description']:
        extractor.extract_keywords_from_text(description)
        keywords = extractor.get_ranked_phrases()
        keywords_temp = keywords.copy()
        for key in keywords_temp:
            if key.isdigit() or key in stop_words:
                keywords.remove(key)
        keywords = normalize_text(keywords)
        bags_words = bags_words + keywords[:5]
    return bags_words

def handle_textual_feature(dataframe, extract_keywords=False):
    if extract_keywords:
        bag_words = create_bags_of_words(dataframe)
        count_words = histogram(bag_words)
        count_words = [(k, v) for k, v in count_words.items()]
        # sort from most appearance
        count_words.sort(key=lambda x: -x[1])
        keywords_feature = [x[0] for x in count_words[:MAX_TEXTUAL_FEATURE]]
        print("the key words that selections :")
        print(keywords_feature)
        with open('keywords.json', 'w') as file:
            json.dump(keywords_feature, file)
        file.close()
    else:
        file = open('keywords.json', 'r')
        keywords_feature = json.load(file)
        file.close()
    for feature in keywords_feature:
        dataframe[feature] = dataframe['description'].apply(lambda x: 1 if feature in x else 0)
    return dataframe

