from nlp_extract import handle_textual_feature
import pandas as pd

def convert_json_to_dataframe(data_file):
    cve_list = []
    for category in data_file:
        for vulnerability in data_file[category]:
            cve_list.append(data_file[category][vulnerability])
    df = pd.DataFrame(cve_list)
    df['metricv3_cvss_vectorString'] = df['metricv3_cvss_vectorString'].apply(
        lambda x: x.replace('CVSS:3.0/', "") if x is not None else x)
    return df

def handle_with_categorical_feature(dataframe):
    feature_dummies = ['cwe','metricv2_cvss_accessComplexity', 'metricv2_cvss_authentication', 'metricv2_cvss_availabilityImpact' , 'metricv2_cvss_vectorString' , 'metricv2_severity','metricv3_cvss_attackComplexity','metricv3_cvss_attackVector','metricv3_cvss_availabilityImpact','metricv3_cvss_baseSeverity','metricv3_cvss_integrityImpact','metricv3_cvss_privilegesRequired', 'metricv3_cvss_vectorString' ,'metricv3_cvss_scope','metricv3_cvss_userInteraction']
    print('num of categorical feature \n ', len(feature_dummies))
    print('categorical feature are: \n', feature_dummies)
    for col in feature_dummies:
        temp_df = pd.get_dummies(dataframe[col])
        dataframe = dataframe.drop(col, axis=1)
        for col_temp in list(temp_df):
            dataframe[col + '_' + col_temp] = temp_df[col_temp]
    return dataframe

def handle_with_boolean_feature(dataframe):
    for col in list(dataframe):
        if isinstance(dataframe[col][0], bool):
            dataframe[col] = dataframe[col].fillna(False)
            dataframe[col] = dataframe[col].astype(int)
    return dataframe

def preprocessing(data_file):
    print("stat convert json format to dataframe")
    dataframe = convert_json_to_dataframe(data_file)
    print("finish convert json format to dataframe")
    print("start feature selection")
    df_feature_selection = dataframe.copy()
    feature_selection = ['Code_Execution', 'cwe', 'description', 'metricv2_acInsufInfo', 'metricv2_cvss_authentication',
                         'metricv2_cvss_availabilityImpact', 'metricv2_cvss_accessComplexity',
                         'metricv2_cvss_baseScore', 'metricv2_exploitability_score', 'metricv2_impact_score',
                         'metricv2_obtainAllPrivilege', 'metricv2_obtainOtherPrivilege', 'metricv2_obtainUserPrivilege',
                         'metricv2_userInteractionRequired', 'metricv2_cvss_vectorString', 'metricv2_severity',
                         'metricv3_cvss_attackComplexity', 'metricv3_cvss_attackVector',
                         'metricv3_cvss_availabilityImpact', 'metricv3_cvss_baseScore', 'metricv3_cvss_baseSeverity',
                         'metricv3_cvss_integrityImpact', 'metricv3_cvss_vectorString',
                         'metricv3_cvss_privilegesRequired', 'metricv3_cvss_scope', 'metricv3_cvss_userInteraction',
                         'metricv3_exploitability_score', 'metricv3_impact_score']

    all_features = list(df_feature_selection)
    drop_features = set(all_features).difference(set(feature_selection))
    print('total number of features : ', len(all_features))
    print('number of features that we select ', len(feature_selection))
    print('the features that we drop \n', drop_features)
    for col in drop_features:
        df_feature_selection = df_feature_selection.drop(col, axis=1)
    df_feature_selection = handle_with_boolean_feature(df_feature_selection)
    print("start handle with textual feature")
    df_feature_selection = handle_textual_feature(df_feature_selection)
    print("finish handle with textual feature")
    df_feature_selection = df_feature_selection.drop('description', axis=1)
    print("start with categorical feature")
    df_feature_selection = handle_with_categorical_feature(df_feature_selection)
    print("finish with categorical feature")
    print(len(list(df_feature_selection)))
    df_feature_selection = df_feature_selection.dropna()
    return df_feature_selection, dataframe